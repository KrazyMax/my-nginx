FROM nginx:latest

# Copy custom configuration files
COPY conf.d/default.conf /etc/nginx/conf.d/

# Set the permissions so nginx can access the necessary files
RUN chown -R nginx:nginx /usr/share/nginx/html

# Expose port 80
EXPOSE 80 443

# Start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]
